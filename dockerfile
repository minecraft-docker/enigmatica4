FROM openjdk:alpine
COPY . /opt
WORKDIR /opt/minecraft

RUN apk add --no-cache bash
RUN apk add --no-cache py-pip
RUN pip install mcstatus

RUN wget https://media.forgecdn.net/files/2937/825/Enigmatica4Server-0.5.4.zip
RUN unzip Enigmatica4Server-0.5.4.zip
RUN rm Enigmatica4Server-0.5.4.zip

RUN cp /opt/eula.txt /opt/minecraft

RUN chmod 700 server-start.sh

HEALTHCHECK --interval=30s --timeout=5s --retries=5 \
   CMD mcstatus localhost ping || exit 1

EXPOSE 25565
ENTRYPOINT ["/opt/minecraft/server-start.sh"]
